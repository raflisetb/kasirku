@extends ('template/default')

@section('content')

 <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                       <center> <h1><i class="icon-archive"></i> Master Data barang</h1>
                        <h4>Tambah, Edit, Hapus Barang</h4></center></center>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Main Content -->
                <!-- Table List Barang --> 
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title" style="background-color: #0090ff">
                                <h3><i class="icon-table"></i> List Barang</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <div class="btn-toolbar pull-right clearfix">
                                    <div class="btn-group">
                                        <a class="btn btn-primary" title="Tambah" href="#tambah"" role="button" data-toggle="modal">Tambah</a>
                                     </div>
                                    <div class="btn-group">

                                        <a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="icon-print"></i></a>
                                        <a class="btn btn-circle show-tooltip" title="Export to PDF" href="#"><i class="icon-file-text-alt"></i></a>
                                        <a class="btn btn-circle show-tooltip" title="Export to Exel" href="#"><i class="icon-table"></i></a>
                                    </div>
                        
                                </div>
                                <div class="clearfix"></div>
<table class="table table-advance" id="table1">
    <thead>
        <tr>
            
 			<th> NO</th>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Stok Barang</th>
            <th>Harga Jual</th>
            <th>Harga Beli</th>
            <th> Aksi </th>
            
        </tr>
    </thead>
    <tbody>
        <tr class="table-flag-blue">
        	 
           <?php 
            $nomor=1;  ?> 

            @foreach ($barangs as $barang)

        	<td><?php echo $nomor++?></td> 
            <td>{{ $barang->kode_brg }}</td>
            <td>{{ $barang->nama_brg }}</td>
            <td>{{ $barang->stok_brg }}</td>
            <td>{{ $barang->harga_jual }}</td>
            <td>{{ $barang->harga_beli }}</td> 
            <td> 
              <a class="btn btn-circle show-tooltip" title="Update" href="#edit{{ $barang->kode_brg }}" role="button" data-toggle="modal"><i class="icon-edit"></i></a>
            </td>

             </tr>
                 <div id="edit{{ $barang->kode_brg }}" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> 
              <form action ="" method="post">
              <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Tambah Barang</h3>
                </div>
               <div class="modal-body">
                {{ csrf_field () }}
                    <label><b>kode Barang</b></label>
                    <input type="hidden" name="kode_brg"e value="{{ $barang->kode_brg }}" />
                    {{ $barang->kode_brg }}
                    <br>
                    <label><b>Nama Barang</b></label>
                    <input type="text" name="nama_brg" value="{{ $barang->nama_brg }}" />
                    <br>
                    <label><b>Stok Barang</b></label>
                    <input type="text" name="stok_brg" value="{{ $barang->stok_brg }}" />
                    <br>
                    <label><b>Harga Jual</b></label>
                   <input type="text" name="harga_jual" value="{{ $barang->harga_jual }}" />
                    <br>
                    <label><b>Harga Beli</b></label>
                   <input type="text" name="harga_beli" value="{{ $barang->harga_beli }}" />
                    <br>      
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">SUBMIT</button>
                    <!-- <input type="submit" value="Simpan"> -->
                   <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>              
                </div>
              </form>
             </div>
              @endforeach
    </tbody>

    

               
            </table>
            </div>
            <div id="tambah" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> 
              <form action ="{{ route('barang.store') }}" method="post">
              <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Edit Data Barang</h3>
                </div>
               <div class="modal-body">
                    {{ csrf_field () }}
                    <label><b>kode Barang</b></label>
                    <input type="hidden" name="kode_brg" value="{{ $kode_brg_baru }}"  />
                    {{ $kode_brg_baru }}
                    <br>
                    <label><b>Nama Barang</b></label>
                    <input type="text" name="nama_brg" />
                    <br>
                    <label><b>Stok Barang</b></label>
                    <input type="text" name="stok_brg" />
                    <br>
                    <label><b>Harga Jual</b></label>
                   <input type="text" name="harga_jual"  />
                    <br>
                    <label><b>Harga Beli</b></label>
                   <input type="text" name="harga_beli" />
                    <br>      
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">SUBMIT</button>
                    <!-- <input type="submit" value="Simpan"> -->
                   <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>              
                </div>
              </form>
             </div>

        </div>
    </div>
</div>

    
   



@endsection