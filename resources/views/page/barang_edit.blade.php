@extends ('template/default')

@section('content')
 <div id="tambah" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> 
              <form action ="{{ route('simpan') }}" method="post">
              <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Edit Data Barang</h3>
                </div>
               <div class="modal-body">
                    {{ csrf_field () }}
                    <label><b>kode Barang</b></label>
                    <input type="text" name="kode_brg"  />
                    <br>
                    <label><b>Nama Barang</b></label>
                    <input type="text" name="nama_brg" />
                    <br>
                    <label><b>Stok Barang</b></label>
                    <input type="text" name="stok_brg" />
                    <br>
                    <label><b>Harga Jual</b></label>
                   <input type="text" name="harga_jual"  />
                    <br>
                    <label><b>Harga Beli</b></label>
                   <input type="text" name="harga_beli" />
                    <br>      
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">SUBMIT</button>
                    <!-- <input type="submit" value="Simpan"> -->
                   <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>              
                </div>
              </form>
             </div>