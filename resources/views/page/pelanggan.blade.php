@extends ('template/default')

@section('content') 
    <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                       <center> <h1><i class="icon-archive"></i>Detail Pelanggan</h1>
                        <h4>Tambah, Edit, Hapus Pelanggan</h4></center></center>
                    </div>
                </div>
                <!-- END Page Title -->



               <!-- Table List pelanggan --> 
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title" style="background-color: #0090ff">
                                <h3><i class="icon-table"></i> List pelanggan</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <div class="btn-toolbar pull-right clearfix">
                                    <div class="btn-group">
                                        <a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="icon-print"></i></a>
                                        <a class="btn btn-circle show-tooltip" title="add" href="#new"><i class="icon-plus"></i></a>
                                        <a class="btn btn-circle show-tooltip" title="Export to PDF" href="#"><i class="icon-file-text-alt"></i></a>
                                        <a class="btn btn-circle show-tooltip" title="Export to Exel" href="#"><i class="icon-table"></i></a>
                                    </div>
                                    <div class="btn-group">
                                        <a class="btn btn-circle show-tooltip" title="Refresh" href="#"><i class="icon-repeat"></i></a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
<table class="table table-advance" id="table1">
    <thead>
        <tr>
            
      <th> NO</th>
            <th>Kode Pelanggan</th>
           
            <th>Nama Pelanggan</th>
            <th>Alamat Pelanggan</th>
            <th>No Hp Pelanggan</th>    
            
        </tr>
    </thead>
    <tbody>
        <tr class="table-flag-blue">
          <?php 
            $nomor=1; ?> 
            
        

          @foreach ($pelanggans as $pelanggan)

          <td><?php echo $nomor++; ?></td> 
            <td>{{ $pelanggan->id_plgn }}</td>
            <td>{{ $pelanggan->nama_plgn }}</td>
            <td>{{ $pelanggan->alamat_plgn }}</td>
            <td>{{ $pelanggan->no_hp_plgn }}</td>
           <td>  <a class="btn btn-circle show-tooltip" title="Update" href="#myModal{{$pelanggan->id_plgn}}" role="button" data-toggle="modal"><i class="icon-edit"></i></a></td>

                     
          </tr>
      </tbody>
         <div id="myModal{{$pelanggan->id_plgn}}" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                   <form action="" method="post">
                <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h3 id="myModalLabel">Edit Data Pelanggan</h3>
                  </div>
                <div class="modal-body">
                    <input type="hidden" name="id_plgn" value="{{ $pelanggan->id_plgn }}" />
                    <label><b>Nama Pelanggan</b></label>
                    <input type="text" name="nama_plgn" value="{{ $pelanggan->nama_plgn }}" />
                    <br>
                    <label><b>Alamat Pelanggan</b></label>
                    <input type="text" name="alamat_plgn" value="{{ $pelanggan->alamat_plgn }}" />
                    <br>
                    <label><b>No Hp Pelanggan</b></label>
                   <input type="text" name="no_hp_plgn" value="{{ $pelanggan->no_hp_plgn }}" />
                    <br>   
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">SUBMIT</button>
                    <!-- <input type="submit" value="Simpan"> -->
                   <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>              
                </div>
            </form>
        </div>
      @endforeach

      </table>
    </div>
  </div>
</div>

  
    <div id="modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                   <form action="" method="post">
                <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h3 id="myModalLabel">Edit Data Pelanggan</h3>
                  </div>
                <div class="modal-body">
                    <input type="hidden" name="id_plgn" value="" />
                    <label><b>Nama Pelanggan</b></label>
                    <input type="text" name="nama_plgn" value=" />
                    <br>
                    <label><b>Alamat Pelanggan</b></label>
                    <input type="text" name="alamat_plgn" value="" />
                    <br>
                    <label><b>No Hp Pelanggan</b></label>
                   <input type="text" name="no_hp_plgn" value="" />
                    <br>   
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">SUBMIT</button>
                    <!-- <input type="submit" value="Simpan"> -->
                   <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>              
                </div>
            </form>
        </div>


<!-- Form  edit pelanggan -->

                   
                 

 <!-- Form  pelanggan Baru -->
              
                <!-- END Main Content -->
                
          
@endsection

