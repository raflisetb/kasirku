@extends ('template/default')

@section('content')

 <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                       <center> <h1><i class="icon-archive"></i> Master Data barang</h1>
                        <h4>Tambah, Edit, Hapus Barang</h4></center></center>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Main Content -->
                <!-- Table List Barang --> 
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box">
                            <div class="box-title" style="background-color: #0090ff">
                                <h3><i class="icon-table"></i> List Barang</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="icon-remove"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <div class="btn-toolbar pull-right clearfix">
                                    <div class="btn-group">
                                        <a class="btn btn-primary" title="Tambah" href="#tambah"" role="button" data-toggle="modal">Tambah</a>
                                     </div>
                                    <div class="btn-group">

                                        <a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="icon-print"></i></a>
                                        <a class="btn btn-circle show-tooltip" title="Export to PDF" href="#"><i class="icon-file-text-alt"></i></a>
                                        <a class="btn btn-circle show-tooltip" title="Export to Exel" href="#"><i class="icon-table"></i></a>
                                    </div>
                        
                                </div>
                                <div class="clearfix"></div>
<table class="table table-advance" id="table1">
    <thead>
        <tr>
            
 			<th> NO</th>
            <th>Kode Barang</th>
            
        </tr>
    </thead>
    <tbody>
        <tr class="table-flag-blue">
        	 
           <?php 
            $nomor=1;  ?> 

            
           
        	<td><?php echo $nomor?></td> 
            <td>{{ $kode_brg_baru }} </td>
            </tr>
           
              
    </tbody>       
            </table>
            </div>
          
        </div>
    </div>
</div>

    
   



@endsection