
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Bangunan Saya</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <!--base css styles-->
        <link rel="stylesheet" href="{{ asset('admin/assets/bootstrap/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('admin/assets/bootstrap/bootstrap-responsive.min.css') }}">
        <link rel="stylesheet" href="{{ asset('admin/assets/font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('admin/assets/normalize/normalize.css') }}">

        <!--page specific css styles-->

        <!--flaty css styles-->
        <link rel="stylesheet" href="{{ asset('admin/css/flaty.css') }}">
        <link rel="stylesheet" href="{{ asset('admin/css/flaty-responsive.css') }}">

        <link rel="shortcut icon" href="{{ asset('admin/img/favicon.html') }}">

        <script src="{{ asset('admin/assets/modernizr/modernizr-2.6.2.min.js') }}"></script>
        <link rel="stylesheet" href="{{ asset('admin/assets/data-tables/DT_bootstrap.css') }}" />
    </head>