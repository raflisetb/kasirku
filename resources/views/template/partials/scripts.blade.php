
    <!--basic scripts-->
        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>-->
        <script>window.jQuery || document.write('<script src="{{ asset("admin/assets/jquery/jquery-1.10.1.min.js") }}"><\/script>')</script>
        <script src="{{ asset('admin/assets/bootstrap/bootstrap.min.js') }}"></script>
        <script src="{{ asset('admin/assets/nicescroll/jquery.nicescroll.min.js') }}"></script>


        <!--page specific plugin scripts-->
        <script src="{{ asset('admin/assets/flot/jquery.flot.js') }}"></script>
        <script src="{{ asset('admin/assets/flot/jquery.flot.resize.js') }}"></script>
        <script src="{{ asset('admin/assets/flot/jquery.flot.pie.js') }}"></script>
        <script src="{{ asset('admin/assets/flot/jquery.flot.stack.js') }}"></script>
        <script src="{{ asset('admin/assets/flot/jquery.flot.crosshair.js') }}"></script>
        <script src="{{ asset('admin/assets/flot/jquery.flot.tooltip.min.js') }}"></script>
        <script src="{{ asset('admin/assets/sparkline/jquery.sparkline.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('admin/assets/jquery-validation/dist/jquery.validate.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('admin/assets/jquery-validation/dist/additional-methods.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('admin/assets/data-tables/jquery.dataTables.js') }}"></script>
        <script type="text/javascript" src="{{ asset('admin/assets/data-tables/DT_bootstrap.js') }}"></script>



        <!--flaty scripts-->
        <script src="{{ asset('admin/js/flaty.js') }}"></script>
