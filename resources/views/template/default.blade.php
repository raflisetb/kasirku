<!DOCTYPE html>
      @include ('template/partials/head')
<body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->


        <!-- BEGIN Navbar -->
      @include ('template/partials/navbar')
        <!-- END Navbar -->

        <!-- BEGIN Container -->
        <div class="container-fluid" id="main-container">
            <!-- BEGIN Sidebar -->
         @include ('template/partials/sidebar')
            <!-- END Sidebar -->

            <!-- BEGIN Content -->
            <div id="main-content">
            

                <!-- BEGIN Main Content -->
              @yield('content')


            </div>
          </div>
          <!-- Javascripts --> 
          @include('template/partials/scripts')
        </body>
</html> 

        