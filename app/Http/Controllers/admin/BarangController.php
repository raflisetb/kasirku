<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Models\Barang;
use App\Http\Controllers\Controller;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barangs = Barang::all(); 
        $ambilkode = Barang::orderBy('kode_brg', 'desc')->get()->first(); 
        $kode_brg = $ambilkode->kode_brg; 
        $last = substr($kode_brg,1,3); 
        $selanjutnya = $last+1;
        $B = "B";
        $kode_brg_baru = $B.sprintf('%03d', $selanjutnya); 
        $kode_brg = $ambilkode->kode_brg; 

        return view('page/barang', compact('barangs', 'kode_brg_baru'));
    }

    public function tes()
    {
     $ambilkode = Barang::orderBy('kode_brg', 'desc')->get()->first(); 
        $kode_brg = $ambilkode->kode_brg; 
        $last = substr($kode_brg,1,3); 
        $selanjutnya = $last+1;
        $B = "B";
        $kode_brg_baru = $B.sprintf('%03d', $selanjutnya); 
        $kode_brg = $ambilkode->kode_brg; 
       
       return view('page/tes', compact('kode_brg_baru'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('page/barang#tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        //


        $barang = Barang::create($request->all());
        return redirect('admin/barang'); 
       
    }

    public function edit($id)
    {
        //
        $barang = Barang::findOrFail($kode_brg); 
        return view('page/barang_edit'); 

    }

  
    public function update(Request $request, $kode_brg)
    {
        $barang = Barang::findOrFail($kode_brg)->update($request->all()); 
        return redirect()->route('page/barang')->with('message', 'Barang berhasil di update'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /*
    public function show($kode_brg)
    {
        $barang = Barang::findOrFall($kode_brg); 
        return view('page/editbarang');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /*
    public function edit($kode_brg)
    {
        // 
         
         $barang = Barang::find($kode_brg); 

         return view('page/editbarang', [
            'barang'=> $barang
        ]); 
         
       
    }

   
    public function update(Request $request, $kode_brg)
    {
        /*
          $barangs = Barang::findOrFall($kode_brg); 
          $barangs->nama_brg =  $request->get('nama_brg'); 
          $barangs->stok_brg = $request->get('stok_brg'); 
          $barangs->harga_jual = $request->get('harga_jual'); 
          $barangs->harga_beli = $request->get('harga_beli'); 

          $barangs->save(); 
          return redirect('page/barang/')->with('Barang Berhasil di update'); 
          */

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function destroy($id)
    {
        //
    }
}
