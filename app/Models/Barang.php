<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table="barang"; 
    public $timestamps=false; 
    protected $fillable = 
    [ 'kode_brg', 'nama_brg', 'nama_brg', 'stok_brg', 'harga_jual', 'harga_beli' ]; 
  
}
